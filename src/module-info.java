module Main {
	requires javafx.controls;
	requires java.sql;
	requires ejml;
	requires org.knowm.xchart;
	requires java.desktop;
	requires javafx.graphics;
	requires javafx.base;
	requires javafx.swing;
	
	opens main to javafx.graphics, javafx.fxml;
}
