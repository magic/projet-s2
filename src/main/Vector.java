package main;
/*
 * Vector class
 *  size = length of vector
 *  data = values of vectors
 *  id = label of corresponding image
 *  
 * Methods : 
 *  - dot
 *  - add
 *  - subtract
 *  - min
 *  - max
 *  - normalise
 */
public class Vector {
	public int size;
	public double[] data;
	public String id = "";
	
	@Override
	public String toString() {
		return "Vector [size=" + size + ", data=" + this.min() + ":" + this.max() + "]";
	}

	public Vector(int n) {
		data = new double[n];
		size = n;
	}
	
	public Vector(double[] d) {
		data = d.clone();
		size = d.length;
	}
	
	public Vector(int[] d) {
		data = new double[d.length];
		size = d.length;
		for (int i=0;i<size;i++) {
			data[i] = (double) d[i];
		}
	}
	
	public Vector(Vector v) {
		id = v.id;
		size = v.size;
		data = v.data.clone();
	}

	public void dot(double x) {
		for (int i=0;i<size;i++) {
			data[i] *= x;
		}
	}
	
	public void dot(Vector v) {
		if (v.size == size) {
			for (int i=0;i<size;i++) {
				data[i] *= v.data[i];
			}
		} else {
			throw new IllegalArgumentException("vector sizes are not equal");
		}
	}
	
	public static double dot(Vector v1, Vector v2) {
		if (v1.size == v2.size) {
			double s = 0;
			for (int i=0;i<v1.size;i++) {
				s += v1.data[i] * v2.data[i];
			}
			return s;
		} else {
			throw new IllegalArgumentException("vector sizes are not equal");
		}
	}
	
	public void add(double x) {
		for (int i=0;i<size;i++) {
			data[i] += x;
		}
	}
	
	public void add(Vector v) {
		if (v.size == size) {
			for (int i=0;i<size;i++) {
				data[i] += v.data[i];
			}
		} else {
			throw new IllegalArgumentException("vector sizes are not equal");
		}
	}
	
	public static Vector add(Vector v1, Vector v2) {
		if (v1.size == v2.size) {
			Vector v = new Vector(v1.size);
			for (int i=0;i<v1.size;i++) {
				v.data[i] = v1.data[i] + v2.data[i];
			}
			return v;
		} else {
			throw new IllegalArgumentException("vector sizes are not equal");
		}
	}

	public void sub(Vector v) {
		if (v.size == size) {
			for (int i=0;i<size;i++) {
				data[i] -= v.data[i];
			}
		} else {
			throw new IllegalArgumentException("vector sizes are not equal");
		}
	}
	
	public double max() {
		double s = data[0];
		for (int i=1;i<size;i++) {
			if (data[i] > s)
				s = data[i];
		}
		return s;
	}
	
	public double min() {
		double s = data[0];
		for (int i=1;i<size;i++) {
			if (data[i] < s)
				s = data[i];
		}
		return s;
	}
	
	public void normalise() {
		double s = 0;
		for (int i=0; i<size; i++) {
			s += data[i];
		}
		for (int i=0; i<size; i++) {
			data[i] = data[i] / s;
		}
	}
	
	public void map(double a,double b,double c,double d) {
		for (int i=0;i<this.size;i++) {
			this.data[i] = (this.data[i] - a) / (b - a) * (d - c) + c;
		}
	}
	
	public static Vector getPixel(Vector v) {
		Vector V = new Vector(v);
		V.map(V.min(), V.max(), 0, 255);
		return V;
	}
}
