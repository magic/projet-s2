package main;

import java.io.File;
import java.util.Random;

import javax.swing.JFrame;
import org.ejml.data.DenseMatrix64F;
import org.ejml.factory.DecompositionFactory;
import org.ejml.interfaces.decomposition.EigenDecomposition;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.markers.SeriesMarkers;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class Main extends Application{
	File BASE = new File(".");
	
	File REF_IMAGES = new File("faces/Refs/");
	Text REF_LABEL = new Text(relative(REF_IMAGES.getPath(), BASE.getPath()));
	File TEST_IMAGES = new File("faces/Tests/");
	Text TEST_LABEL = new Text(relative(TEST_IMAGES.getPath(), BASE.getPath()));
	
	Vector AVG_VECTOR;
	Matrix EIGENFACES;
	double[] EIGENVALUES;
	double THRESHOLD;
	Text THRESHOLD_LABEL = new Text("Threshold");
	double RATIO;
	Text RATIO_LABEL = new Text("Ratio");
	
	Vector SELECTED_IMAGE = null;
	ImageView SELECTED_VIEW = new ImageView();
	Vector PROJECTED_IMAGE = null;
	ImageView PROJECTED_VIEW =  new ImageView();
	Vector CLOSEST_IMAGE = null;
	ImageView CLOSEST_VIEW =  new ImageView();
	double IMAGE_DISTANCE = 0;
	Text IMAGE_DISTANCE_LABEL = new Text("Error");
	Slider slider = new Slider(1, 10, 10/2);;
	
	int W,H,N,NK;
	
	/*
	 * Main function to generate the IHM
	 * Uses javafx to generate a scene
	 */
	@Override
	public void start(Stage primaryStage) {
		update();
		try {
			BorderPane root = new BorderPane();
			VBox leftPane = new VBox();
			VBox rightPane = new VBox();
			leftPane.setPadding(new Insets(10));
			leftPane.setSpacing(10);
			rightPane.setPadding(new Insets(10));
			rightPane.setSpacing(10);
		
			VBox ref_cont = new VBox();
			Button choose_refs = new Button("Ref Images");
			choose_refs.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					DirectoryChooser chooser = new DirectoryChooser();
					chooser.setTitle("Reference Images");

					File defaultDirectory = new File("faces/");
					chooser.setInitialDirectory(defaultDirectory);

					REF_IMAGES = chooser.showDialog(primaryStage);
					update();
				}
				
			});
			ref_cont.getChildren().add(choose_refs);
			ref_cont.getChildren().add(REF_LABEL);
			leftPane.getChildren().add(ref_cont);
			

			VBox test_cont = new VBox();
			Button choose_tests = new Button("Test Images");
			choose_tests.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					DirectoryChooser chooser = new DirectoryChooser();
					chooser.setTitle("Test Images");

					File defaultDirectory = new File("faces/");
					chooser.setInitialDirectory(defaultDirectory);

					TEST_IMAGES = chooser.showDialog(primaryStage);
					update();
				}
			});
			test_cont.getChildren().add(choose_tests);
			test_cont.getChildren().add(TEST_LABEL);
			leftPane.getChildren().add(test_cont);
			
			HBox image_cont = new HBox();
			Button select_image = new Button("Select Image");
			select_image.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					FileChooser fileChooser = new FileChooser();
					fileChooser.setTitle("Open Resource File");

					fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Image Files", "*.png", "*.jpg"));

					File selectedFile = fileChooser.showOpenDialog(primaryStage);
					Image i = new Image(selectedFile.getPath());
					SELECTED_IMAGE = i.toVector();
					SELECTED_IMAGE.map(0, 255, 0, 1);
					updateImage(NK);
				}
			});
			//image_cont.getChildren().add(select_image);
			leftPane.getChildren().add(image_cont);
			
			root.setLeft(leftPane);
			
			Button showEigenfaces = new Button("Show EigenFaces");
			showEigenfaces.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					showEigenFaces();					
				}
				
			});
			rightPane.getChildren().add(showEigenfaces);
			
			Button showNormalised = new Button("Show EigenValues");
			showNormalised.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					showNormalisedValues();
				}
				
			});
			rightPane.getChildren().add(showNormalised);
			
			Button showDistances = new Button("Show Distances");
			showDistances.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					showDistances();
				}
				
			});
			rightPane.getChildren().add(showDistances);
			rightPane.getChildren().add(THRESHOLD_LABEL);
			rightPane.getChildren().add(RATIO_LABEL);
			
			root.setRight(rightPane);
			
			VBox center = new VBox();
			center.setPadding(new Insets(10));
			center.getChildren().add(select_image);
			HBox selected_image = new HBox();
			selected_image.getChildren().add(SELECTED_VIEW);
			selected_image.getChildren().add(new Text("Selected Image"));
			center.getChildren().add(selected_image);
			
			HBox projected_image = new HBox();
			projected_image.getChildren().add(PROJECTED_VIEW);
			VBox a = new VBox();
			a.getChildren().add(new Text("Projected Image"));
			a.getChildren().add(IMAGE_DISTANCE_LABEL);
			projected_image.getChildren().add(a);			
			center.getChildren().add(projected_image);
			
			HBox closest_image = new HBox();
			closest_image.getChildren().add(CLOSEST_VIEW);
			closest_image.getChildren().add(new Text("Closest Image"));
			center.getChildren().add(closest_image);
			
			VBox s = new VBox();
			s.getChildren().add(new Text("K: "));
			
			slider.valueProperty().addListener(new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> arg0, Number oV, Number nV) {
					updateImage(nV.intValue());
					NK = nV.intValue();
					slider.setValue((double)nV.intValue());
				}
 
		    });
			s.getChildren().add(slider);
			center.getChildren().add(s);
			center.setSpacing(10);
			
			root.setCenter(center);
			
			Scene scene = new Scene(root, 600, 600);
			scene.getStylesheets().add(getClass().getResource("main.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Eigenfaces");

			System.out.println("Loaded!");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Function that updates the images, eigenfaces, ratio and threshold based on the reference and test images
	 */
	public void update() {
		REF_LABEL.setText(relative(REF_IMAGES.getPath(), BASE.getPath()));
		TEST_LABEL.setText(relative(TEST_IMAGES.getPath(), BASE.getPath()));
		calculateEigenFaces();
		calculateRatioThreshold();
		THRESHOLD_LABEL.setText("Seuil: " + String.valueOf(Math.round(THRESHOLD * 100.0) / 100.0));
		RATIO_LABEL.setText("Ratio: " + String.valueOf(Math.round(RATIO * 100.0) / 100.0) + "%");
		saveDistances();
		slider.setMax(N);
		slider.setBlockIncrement(1.0);
		slider.setShowTickMarks(true);
		slider.setShowTickLabels(true);
		slider.setMajorTickUnit(3.0);
		slider.setSnapToTicks(true);
	}
	
	/*
	 * Function that calculates the eigenfaces
	 */
	public void calculateEigenFaces() {
		Vector[] vector_images = getImages(REF_IMAGES.getPath());
		W = (int) Math.sqrt(vector_images[0].size);
		H = (int) Math.sqrt(vector_images[0].size);
		N = vector_images.length;
		NK = N;
		
		Vector avg_vector = calculateAvg(vector_images);
		// save the avg image
		Image.save(new Vector(Vector.getPixel(avg_vector)), W, H, "avg_image.png");
		// Set global variable of avg image
		AVG_VECTOR = new Vector(avg_vector);	
		
		Matrix A = new Matrix(vector_images, N);
		
		// get eigenvalues and eigenvectors
		EigenDecomposition<DenseMatrix64F> evd = decomposeMatrix(A);
		
		// refactor decomposition
		EIGENVALUES = new double[N];
		Matrix EigenVectors = new Matrix(N,N);
		
		for (int i=0;i<N;i++) {
			EIGENVALUES[i] = evd.getEigenvalue(i).real;
			EigenVectors.data[i] = evd.getEigenVector(i).data;
		}
		
		// calculate Eigenfaces
		EIGENFACES = Matrix.matrixMultiply(A, EigenVectors);
		EIGENFACES.normalise();
				
		// Sort Eigenfaces based on eigenvalues
		int swaps = 0;
		do {
			swaps = 0;
			for (int i=0;i<EigenVectors.cols-1;i++) {
				if (EIGENVALUES[i] > EIGENVALUES[i+1]) {
					double t = EIGENVALUES[i];
					EIGENVALUES[i] = EIGENVALUES[i+1];
					EIGENVALUES[i+1] = t;
					EigenVectors.swap(i, i+1);
					swaps += 1;
				}
			}
		}while (swaps != 0);
		
		// save the eigenfaces to Eigenfaces/
		saveEigenfaces();
	}
	
	/*
	 * function that calculates the average vector
	 */
	public Vector calculateAvg(Vector[] vectors) {
		Vector v = new Vector(vectors[0].size);
		for (int i=0;i<vectors.length; i++) {
			v.add(vectors[i]);
		}
		v.dot(1.0 / vectors.length);
		return v;
	}
	
	/*
	 * Function that decomposes a square matrix in to its eigenvectors and eigenvalues
	 * uses the Efficent Java Matrix Libary (EJML)
	 */
	public EigenDecomposition<DenseMatrix64F> decomposeMatrix(Matrix A) {
		// calculate comatrix 
		Matrix C = Matrix.matrixMultiply(Matrix.Transpose(A), A);
		DenseMatrix64F DMc = new DenseMatrix64F(C.data);
		EigenDecomposition<DenseMatrix64F> evd = DecompositionFactory.eig(C.cols, true, false);
		evd.decompose(DMc);
		return evd;
	}
	
	/*
	 * saves the eigenfaces to "Eigenfaces/"
	 */
	public void saveEigenfaces() {
		for (int i=0;i<N;i++) {
			Vector vs= new Vector(EIGENFACES.data[N-i-1]);
			vs.map(vs.min(), vs.max(), 0, 255);
			Image.save(vs, W, H, "EigenFaces/"+i+".png");
		}
	}

	/*
	 * returns the k-eigenface from the given eigenvectors
	 */
	public Matrix reduceSpace(int K, Matrix EigenFaces) {
		Matrix F =  new Matrix(K, EigenFaces.rows);
		for (int i=0;i<K;i++) {
			F.data[i] = EigenFaces.data[EigenFaces.data.length-i-1].clone();
		}
		return F;
	}
	
	/*
	 * calculates the distance between two vectors
	 */
	public double getError(Vector v1, Vector v2) {
		double d=0;
		for (int i=0; i<v1.size;i++) {
			d += (v2.data[i] - v1.data[i]) * (v2.data[i] - v1.data[i]);
		}
		return Math.sqrt(d);
	}
	
	/*
	 * returns the projected vector v2 in the eigenspace F
	 */
	public Vector ProjectImage(Vector v2, Vector avg_vector, Matrix F, int K) {
		v2.sub(avg_vector);
		Matrix w = Matrix.matrixMultiply(F, Matrix.matrixMultiply(Matrix.Transpose(F), new Matrix(v2)));
		Vector v = Vector.add(avg_vector, new Vector(w.data[0]));
		return v;
	}

	/*
	 * finds and returns the closest image (vector) to a given image for a value of K
	 */
	public Vector findClosestImage(Vector test_image, Vector avg_vector, Matrix F, int K) {
		Vector v = null;
		double e_min = 1000000.0;
		Vector[] ref_images = getImages(REF_IMAGES.getPath());
		for (int i=0; i<ref_images.length;i++) {
			double e = getError(ProjectImage(new Vector(ref_images[i]), avg_vector, F, K), test_image);
			if (e < e_min) {
				e_min = e;
				v = new Vector(ref_images[i]);
			}
		}
		return v;
	}
	
	/*
	 * function used for generating relative paths
	 */
	public String relative(String path, String base) {
		return new File(base).toURI().relativize(new File(path).toURI()).getPath();
	}
	
	/*
	 * randomises the order of files in a directory
	 */
	private void shuffleFileArray(File[] array)
	{
	    int index;
	    File t;
	    Random random = new Random();
	    for (int i = array.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        if (index != i)
	        {
	            t = array[i];
	            array[i] = array[index];
	            array[index] = t;
	        }
	    }
	}
	
	/*
	 * returns the normalized vectors of each image in a directory
	 */
	public Vector[] getImages(String path_images) {
		// get list of image paths
		File image_folder = new File(relative(path_images, BASE.getPath()));
		File[] list_images = image_folder.listFiles();
		shuffleFileArray(list_images);
		
		// number of images
		int N = list_images.length;
		
		Vector[] vector_images = new Vector[N];
		for (int i=0;i<N;i++) {
			Image img = new Image(path_images+"/"+list_images[i].getName());
			vector_images[i] = img.toVector();
			vector_images[i].map(0, 255, 0, 1);
		}
		return vector_images;
	}

	/*
	 * functions to show images using jframe
	 */
	public void showVectorAsImage(Vector v, String t, int r, int c) {
		new ImageViewer(new Image(Vector.getPixel(v)), t, r, c);
	}
	
	public void showVectorsAsImages(Vector[] vs, int n, int r, int c) {
		Image[] imgs = new Image[Math.min(n,  vs.length)];
		for (int i=0; i<Math.min(n,  vs.length);i++) {
			imgs[i] = new Image(Vector.getPixel(vs[i]));
		}
		new ImageViewer(imgs, r, c);
	}
	
	public void showVectorsAsImages(Vector[] vs, int n,String t, String[] ts, int r, int c) {
		Image[] imgs = new Image[Math.min(n,  vs.length)];
		for (int i=0; i<Math.min(n,  vs.length);i++) {
			imgs[i] = new Image(Vector.getPixel(vs[i]));
		}
		new ImageViewer(imgs, t, ts, r, c);
	}
	
	/*
	 * function to show the graph of the sum of normalized eigenvalues
	 */
	public void showNormalisedValues() {
		// normalise eigenvalues
		Vector EV = new Vector(EIGENVALUES);
		EV.normalise();
		double[] kx = new double[EV.size];
		double[] ky = new double[EV.size];
		double s=0;
		for (int i=0;i<EV.size;i++) {
			s += EV.data[EV.size-1-i];
			kx[i] = i;
			ky[i] = s;
		}

		XYChart chart2 = new XYChartBuilder().width(800).height(600).xAxisTitle("K").yAxisTitle("Sum of EigenValues").build();
		chart2.addSeries("sum", kx, ky).setMarker(SeriesMarkers.NONE);

		@SuppressWarnings({ "unchecked", "rawtypes" })
		JFrame frame = new SwingWrapper(chart2).displayChart();
		javax.swing.SwingUtilities.invokeLater(
				()->frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)
		);
	}

	/*
	 * function that plots and shows the distances of reference and test images based on different values of K
	 */
	public void showDistances() {
		int L = Math.min(N,  5);
		
		double[][] xData = new double[L*2][N-1];
		double[] yData = new double[N-1];
		Vector[] ref_images = getImages(REF_IMAGES.getPath());
		Vector[] test_images = getImages(TEST_IMAGES.getPath());
		for (int l=1; l<N; l++) {
			int K = l;
			yData[l-1] = K;
			// caluclaute reduced eigenspace + save eigenfaces
			Matrix F = reduceSpace(K, new Matrix(EIGENFACES.data));
			
			// reference Images
			for (int i=0;i<L;i++) {
				Vector v = ProjectImage(new Vector(ref_images[i]), AVG_VECTOR, new Matrix(F.data), K);
				xData[i][l-1] = getError(v, ref_images[i]);
			}
			// test_images
			
			for (int i=0;i<L;i++) {
				Vector v = ProjectImage(new Vector(test_images[i]), AVG_VECTOR, F, K);
				xData[L+i][l-1] = getError(v, test_images[i]);
			}

		}
		

		// Create Chart
		XYChart chart = new XYChartBuilder().width(800).height(600).xAxisTitle("K").yAxisTitle("E(J)").build();
		for (int m=0; m<L*2; m++) {
			chart.addSeries(String.valueOf(m), yData, xData[m]).setMarker(SeriesMarkers.NONE);
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		JFrame frame = new SwingWrapper(chart).displayChart();
		javax.swing.SwingUtilities.invokeLater(
				()->frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)
		);
	}
	
	/*
	 * function delete
	 */
	public static void deleteFiles(File dirPath) {
	      File filesList[] = dirPath.listFiles();
	      for(File file : filesList) {
	       if(file.isFile()) {
	            file.delete();
	         } else {
	            deleteFiles(file);
	         }
	      }
		}

	/*
	 * Save the projected images to "comp/"
	 */
	public void saveDistances() {
		deleteFiles(new File("comp/"));
		int[] Ks = new int[5];
		for (int o=1; o<=5;o++) {
			Ks[o-1] = (int)Math.ceil((N*o) / 5.0);
		}

		int L = Math.min(10, N);
		
		Vector[] ref_images = getImages(REF_IMAGES.getPath());
		Vector[] test_images = getImages(TEST_IMAGES.getPath());
		for (int l=0; l<Ks.length; l++) {
			int K = Ks[l];
			// caluclaute reduced eigenspace + save eigenfaces
			Matrix F = reduceSpace(K, new Matrix(EIGENFACES.data));
			// reference Images
			for (int i=0;i<L;i++) {
				Vector v = ProjectImage(new Vector(ref_images[i]), AVG_VECTOR, new Matrix(F.data), K);
				v.map(v.min(), v.max(), 0, 255);
				ref_images[i].map(ref_images[i].min(), ref_images[i].max(), 0, 255);
				Image.save(v, W, H, "comp/ref_"+i+"_k:"+K+".png");
				Image.save(ref_images[i], W, H, "comp/ref_"+i+"_J.png");

			}
			// test_images
			
			for (int i=0;i<L;i++) {
				Vector v = ProjectImage(new Vector(test_images[i]), AVG_VECTOR, F, K);
				v.map(v.min(), v.max(), 0, 255);
				test_images[i].map(test_images[i].min(), test_images[i].max(), 0, 255);
				Image.save(v, W, H, "comp/test_"+i+"_k:"+K+".png");
				Image.save(test_images[i], W, H, "comp/test_"+i+"_J.png");

			}
		}
	}
	
	/*
	 * show the eigenfaces with jframe
	 */
	public void showEigenFaces() {
		Vector[] vs = new Vector[N];
		String[] ts = new String[N]; 
		for (int i=0;i<N;i++) {
			vs[i] = new Vector(EIGENFACES.data[N-i-1]);
			vs[i].map(vs[i].min(), vs[i].max(), 0, 255);
			Image.save(vs[i], W, H, "EigenFaces/"+i+".png");
			ts[i] = String.valueOf(Math.round(EIGENVALUES[N-i-1] * 100.0) / 100.0);
		}
		// show eigenfaces
		showVectorsAsImages(vs, 5, "EigenValues", ts, 0, 2);
	}
	
	/*
	 * Calculate the ratio of correct guesses and distance threshold
	 */
	public void calculateRatioThreshold() {
		int K = N;
		
		Matrix F = reduceSpace(K, EIGENFACES);
		Vector[] test_imgs = null;
		Vector test_image = null;
		Vector closest_image = null;
		Vector projected_image = null;
		double worst_correct_dist = 0.0;
		double best_incorrect_dist = 1000000.0;
		int correct_images = 0;
		int tested_images = 0;
		
		// ref image
		test_imgs = getImages(TEST_IMAGES.getPath());
		
		for (int i=0; i<test_imgs.length;i++) {
			tested_images++;
			test_image = test_imgs[i];
			projected_image = ProjectImage(new Vector(test_image), AVG_VECTOR, F, K);
			closest_image = findClosestImage(new Vector(projected_image), AVG_VECTOR, F, K);
			double e = getError(test_image, projected_image);
			if (test_image.id.equals(closest_image.id)) {
				correct_images++;
				// guessed correctly
				if (worst_correct_dist < e) {
					worst_correct_dist = e;
				}
			} else {
				// guessed wrongly
				if (best_incorrect_dist > e) {
					best_incorrect_dist = e;
				}
			}
		}
		THRESHOLD = (worst_correct_dist + best_incorrect_dist) / 2.0;
		if (THRESHOLD == (1000000.0 / 2.0))
			THRESHOLD = worst_correct_dist;
		RATIO = ((double) correct_images / (double) tested_images) * 100.0;
	}
	
	/*
	 * update the selected, projected and closest image
	 */
	public void updateImage(int K) {
		if (SELECTED_IMAGE != null) {
			SELECTED_VIEW.setImage(SwingFXUtils.toFXImage(new Image(SELECTED_IMAGE).toBufferedImage(), null));
			PROJECTED_IMAGE = ProjectImage(new Vector(SELECTED_IMAGE), AVG_VECTOR, reduceSpace(K, EIGENFACES), K);
			PROJECTED_VIEW.setImage(SwingFXUtils.toFXImage(new Image(PROJECTED_IMAGE).toBufferedImage(), null));
			CLOSEST_IMAGE = findClosestImage(new Vector(PROJECTED_IMAGE), AVG_VECTOR, reduceSpace(K, EIGENFACES), K);
			CLOSEST_VIEW.setImage(SwingFXUtils.toFXImage(new Image(CLOSEST_IMAGE).toBufferedImage(), null));
			IMAGE_DISTANCE = getError(SELECTED_IMAGE, PROJECTED_IMAGE);
			IMAGE_DISTANCE_LABEL.setText("DIst: " + String.valueOf(Math.round(IMAGE_DISTANCE * 100.0) / 100.0));
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Loading...");
		launch(args);
	}
}
