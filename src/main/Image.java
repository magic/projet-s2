package main;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class Image { //class traitement de l'image 
	public int[][] pixels; // matrice de notre image
	public int width; // variable largeur 
	public int height; // variable hauteur
	public String name; //label de l'image
	
	public Image(String filepath) { //pour creer l'objet image a partir d'un fichier
		name = filepath.replaceAll("^.*[\\/\\\\]", ""); //nettoie le chemin
		name = name.substring(0, name.length() - 5); //recupere le label de l'image
		BufferedImage img = null; //initialisation de l'image
		//pour ouvrir le fichier
		try { 
			img = ImageIO.read(new File(filepath)); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (img == null) {
			throw new IllegalArgumentException("something went wrong");
		}
		int c;
		width = img.getWidth(); //recupere largeur de l'image
		height = img.getHeight(); //recupere hauteur de l'image
		pixels = new int[height][width]; //initialisation de la matrice de pixel
		for (int i = 0; i < height; i++) { //pour chaque pixel on recupere la couleur a cet endroit et on colorise en gris 
			for (int j = 0; j < width; j++) {
				c = img.getRGB(j, i); //recuperation couleur
				pixels[i][j] = c & 0xff; //on extrait le gris
			}
		}
	}
	
	public Image(Vector V) { //creation d'une image a partir d'un vecteur, fonction inverse
		Vector v = new Vector(V); //variable vecteur 
		v.map(v.min(), v.max(), 0, 255); //transformation en pixel 
		int w = (int) Math.sqrt(v.size); //on retrouve les largeurs et hauteurs
		int h= w; 
		if (v.max() < 256 && v.min() >= 0) { 
			width = w;
			height = h;
			pixels = new int[height][width];
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					pixels[i][j] = (int) v.data[i*width+j]; //on recréé la matrice a partir du vecteur 
				}
			}
		} else {
			System.out.println(v+"\n");
			throw new IllegalArgumentException("vector is not normalised"); //problème 
		}
	}
	
	public Image(Vector V, int w, int h) { //creation d'une image a partir d'un vecteur, fonction inverse
		Vector v = new Vector(V); //variable vecteur 
		//v.map(v.min(), v.max(), 0, 255); //transformation en pixel 
		if (v.max() < 256 && v.min() >= 0) {
			width = w;
			height = h;
			pixels = new int[height][width];
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					pixels[i][j] = (int) v.data[i*width+j];
				}
			}
		} else {
			System.out.println(v+"\n");
			throw new IllegalArgumentException("vector is not normalised");
		}
	}
	
	public Vector toVector() {  //transformer de l'image en vecteur 
		double[] d = new double[height*width]; //matrice image
		for (int i = 0; i < height; i++) { //boucle qui prends chaque pixel et le transforme en composante(valeurs) dans le vecteur 
			for (int j = 0; j < width; j++) {
				d[i*width+j] = (double) pixels[i][j];
			}
		}
		Vector v = new Vector(d); //
		v.id = name; //set le id du vecteur = label de l'image
		return v; //retroune le vecteur 
	}
	
	public void save(String path) { //sauvegarder l'image 
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB); //creer une image, la remplit avec les bons pixels et ecrit dans un fichier 
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int g = pixels[i][j];
				try {
					img.setRGB(j, i, new Color(g, g, g).getRGB());
				} catch (Exception e) {
					System.out.println(g);
					e.printStackTrace();
				}				
			}
		}
		File f = null;
        try {
            f = new File(path);
            ImageIO.write(img, "png", f);
        }
        catch(Exception e) {
            System.out.println("Error: " + e);
        }
	}
	
	public BufferedImage toBufferedImage() { //pour le type bufferedImage a partir d'une matrice de pixel 
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int g = pixels[i][j];
				try {
					img.setRGB(j, i, new Color(g, g, g).getRGB());
				} catch (Exception e) {
					System.out.println(g);
					e.printStackTrace();
				}				
			}
		}
		return img;
	}
	
	public static void save(Vector v, int w, int h, String path) { //sauvergarder un vecteur en tant qu'image 
		Image i = new Image(v, w, h);
		i.save(path);
	}
}
