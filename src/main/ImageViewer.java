package main;
/*
 * Class ImageViewer
 * uses JFrame to show an image
 * 
 * Methods:
 *  - show single image
 *  - show multiple images
 */


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImageViewer extends JFrame {
	
	private static final long serialVersionUID = 1L;
	JFrame frame = new JFrame();
	
	public void setup(int r, int c) {
		if (r == -1) {
			frame.setLayout(new FlowLayout());
		} else {
			frame.setLayout(new GridLayout(r, c));
		}
	}

	public ImageViewer(BufferedImage img, String t, int r, int c) {
		setup(r,c);
		addText(t);
		addImage(scale(img, 4.0));
		show();
	}
	
	public ImageViewer(Image image, String t, int r, int c) {
		setup(r,c);
		addText(t);
		Image[] images = {image};
		BufferedImage[] imgs = getImages(images);
		addImage(imgs[0]);
		show();
	}
	
	public ImageViewer(Image[] images, String t, String[] ts, int r, int c) {
		setup(r,c);
		addText(t);
		addText("imgs");
		BufferedImage[] imgs = getImages(images);
		showImages(imgs, ts);
		show();
	}
	
	public ImageViewer(Image[] images, int r, int c) {
		setup(r,c);
		BufferedImage[] imgs = getImages(images);
		showImages(imgs);
		show();
	}
	
	
	public BufferedImage[] getImages(Image[] images) {
		BufferedImage[] imgs = new BufferedImage[images.length];
		for (int k=0; k<images.length; k++) {
			imgs[k] = new BufferedImage(images[k].width, images[k].height, BufferedImage.TYPE_INT_ARGB);
			for (int i = 0; i < images[k].height; i++) {
				for (int j = 0; j < images[k].width; j++) {
					int g = images[k].pixels[i][j];
					try {
						imgs[k].setRGB(j, i, new Color(g, g, g).getRGB());
					} catch (Exception e) {
						System.out.println(g);
						e.printStackTrace();
					}				
				}
			}
		}
		return imgs;
	}
	
	public void show() {
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public BufferedImage scale(BufferedImage before, double scale) {
	    int w = before.getWidth();
	    int h = before.getHeight();
	    // Create a new image of the proper size
	    int w2 = (int) (w * scale);
	    int h2 = (int) (h * scale);
	    BufferedImage after = new BufferedImage(w2, h2, BufferedImage.TYPE_INT_ARGB);
	    AffineTransform scaleInstance = AffineTransform.getScaleInstance(scale, scale);
	    AffineTransformOp scaleOp = new AffineTransformOp(scaleInstance, AffineTransformOp.TYPE_BILINEAR);

	    Graphics2D g2 = (Graphics2D) after.getGraphics();
	    // Here, you may draw anything you want into the new image, but we're
	    // drawing a scaled version of the original image.
	    g2.drawImage(before, scaleOp, 0, 0);
	    g2.dispose();
	    return after;
	}
	
	public void addText(String text) {
		JLabel label1 = new JLabel("text");
		label1.setText(text);
		frame.getContentPane().add(label1);
	}
	
	public void addImage(BufferedImage img) {
		frame.getContentPane().add(new JLabel(new ImageIcon(img)));
	}
	
	public void showImages(BufferedImage[] imgs, String[] ts) {
		for (int i=0; i<imgs.length;i++) {
			addText(ts[i]);
			addImage(scale(imgs[i], 2.0));
		}
	}
	
	public void showImages(BufferedImage[] imgs) {
		for (int i=0; i<imgs.length;i++) {
			addImage(scale(imgs[i], 2.0));
		}
	}
}
