package main;
/*
 * Class Matrix
 *  cols = columns of matrix
 *  rows = rows of matrix
 *  
 * Methods:
 *  - create matrix from array, vectors, vector
 *  - multiply
 *  - Normalize
 *  - swap
 */
import java.util.Arrays;

public class Matrix {
	public int cols;
	public int rows;
	public double[][] data;
	
	public Matrix(int c, int r) {
		rows = r;
		cols = c;
		data = new double[cols][rows];
	}
	
	public Matrix (double[][] d) {
		rows = d[0].length;
		cols = d.length;
		data = new double[cols][rows];
		for (int i=0;i<cols;i++) {
			for (int j=0;j<rows;j++) {
				data[i][j] = d[i][j];
			}
		}
	}
	
	public Matrix(Vector[] vectors) {
		cols = vectors.length;
		rows = vectors[0].data.length;
		data = new double[cols][rows];
		for (int i=0; i < cols; i++) {
			data[i] = vectors[i].data.clone();
		}
	}
	
	public Matrix(Vector[] vectors, int N) {
		cols = N;
		rows = vectors[0].data.length;
		data = new double[cols][rows];
		for (int i=0; i < cols; i++) {
			data[i] = vectors[i].data.clone();
		}
	}
	
	
	public Matrix(Vector v) {
		cols=1;
		rows = v.size;
		data = new double[1][rows];
		data[0] = v.data;
	}
	
	@Override
	public String toString() {
		if (cols < 10 && rows < 10) {
			return "Matrix [cols=" + cols + ", rows=" + rows + ", data=" + Arrays.deepToString(data) + "]";
		} else if (rows < 100){
			return "Matrix [cols=" + cols + ", rows=" + rows + ", data=" + Arrays.toString(data[0]) + "]";
		} else {
			String s = "Matrix [cols=" + cols + ", rows=" + rows + ", data=";
			for (int i=0;i<20;i++) {
				s += data[0][i] + ", ";
			}
			s += "]";
			return s;
			
		}
	}

	public static Matrix matrixMultiply(Matrix A, Matrix B) {
		Matrix C = null;
		if (A.cols == B.rows) {
			C = new Matrix(B.cols, A.rows);
			int i,j,k;
			for (i=0;i<C.cols;i++) {
				for (j=0;j<C.rows;j++) {
					for (k=0;k<A.cols;k++) {
						C.data[i][j]+=A.data[k][j]*B.data[i][k];
					}
				}
			}
		} else {
			System.out.println("Invalid sizes for matrix multiplication: \n" + A + "\n" + B);
		}
		return C;
	}
	
	public void normalise() {
		for (int i=0; i<cols;i++) {
			double d = 0;
			for (int j=0;j<rows;j++) {
				d += data[i][j] * data[i][j];
			}
			for (int j=0;j<rows;j++) {
				data[i][j] = data[i][j] / Math.sqrt(d);
			}
		}
	}

	public static Matrix Transpose(Matrix M) {
		Matrix T = new Matrix(M.rows, M.cols);
		for (int i=0;i<M.rows;i++) {
			for (int j=0;j<M.cols;j++) {
				T.data[i][j] = M.data[j][i];
			}
		}
		return T;
	}
	
	public void swap(int i, int j) {
		double[] tmp = data[i].clone();
		data[i] = data[j];
		data[j] = tmp;
	}

}
